// constants and configuration information

export const port = 3000
export const mongoUrl = "mongodb://math_tutor_admin:jack123jill@localhost";
export const database = 'hackathon_jobs'
export const jobsCollection = 'jobs_dev2'
export const applicationCollection = 'job_applications_dev2'
